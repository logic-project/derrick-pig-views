var serverUrl="http://58.250.17.31:8067/v1/";
var opServerUrl=serverUrl+"op/";

var authToken=localStorage.getItem("authToken");

function getUrlParam(name) {
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
	var r = window.location.search.substr(1).match(reg); //获取url中"?"符后的字符串并正则匹配
	var context = "";
	if (r != null)
	context = r[2];
	reg = null;
	r = null;
	return context == null || context == "" || context == "undefined" ? "" : context;
}

layui.use(['layer','element'], function() {
	var $ = layui.jquery,
    layer = layui.layer,
    element = layui.element;

	$.ajaxSetup({
	    complete: function (request, status) {
	        if (typeof (request) != 'undefined') {
	        	if(request.statusText=="error"){
	        		layer.msg("请求异常", {icon:2});
	        		return;
	        	}
	            var code = request.responseJSON?request.responseJSON.code:"";
	            if(code && code!='200'){
	            	if(code == '408'){
	            		layer.msg(request.responseJSON.msg, {icon:2,time:1000},function(){
	            			location.href="/derrick-pig-views/page/main/common/login.html"
	            		});
	            	}else{
	            		layer.msg(request.responseJSON.msg, {icon:2});
	            	}
	            }
	        }
	    },
	    error: function (jqXHR, textStatus, errorThrown) {
	    	var errorText = '';
	    	if(jqXHR.responseJSON){
	    		errorText = jqXHR.responseJSON.error;
	    	}
	        switch (jqXHR.status) {
	            case (500):
	                errorText = '系统异常';
	                break;
	            case (400):
	            	errorText = '请求参数错误';
	                break;
	            case (401):
	                break;
	            case (403):
	                errorText = '您没有访问权限';
	                break;
	            case (404):
	                errorText = '请检查您的访问地址是否存在';
	                break;
	            case (405):
					errorText = '请检查您的请求方式是否错误';
	                break;
	            default:
	            	break;
	        }
	        if (jqXHR.status > 200){
	        	layer.msg(errorText,{icon:2});
	        }
	    }
	});
});
