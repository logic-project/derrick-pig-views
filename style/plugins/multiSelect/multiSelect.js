
$.fn.extend({
	multiSelect : function (obj) {
		var field=this.selector;
		$(field).hide();
		var label=obj.label;
		var data=obj.data;
		var placeholder=obj.placeholder;
		if(!placeholder){
			placeholder="请选择...";
		}
		var style=obj.style;
		var sel_txt='';
		var selnames;
		var selvalues;
		if(data!=null && data!=""){
			selnames=data.name;
			selvalues=data.value;
			if(selnames!=null && selnames!=""
				&& selvalues!=null && selvalues!=""){
				for(var i=0;i<selnames.length;i++){
					sel_txt+='<li class="mt_select_li">'+
								'<a class="mt_select_close" href="javascript:;"></a>'+
								'<label val="'+selvalues[i]+'">'+selnames[i]+'</label>'+
							'</li>';
				}
			}
		}
		
		var labelHtml="";
		if(label){
			labelHtml='<label class="mt_option_li_label">'+label+'</label>';
		}
		var multiHtml='<div class="mt_select_container">'+
			'<div class="mt_select_ful">'+
				'<ul class="mt_select_ul">'+sel_txt+
					'<li class="mt_select_active">'+
						'<input class="mt_select_input" maxlength="0" type="text" name="">'+
					'</li>'+
				'</ul>'+
			'</div>'+
			'<div class="mt_option_container">'+
				'<ul class="mt_option_ul">'+labelHtml+'</ul>'+
			'</div>'+
		'</div>';
		var selects=$(field);
		if(selects!=null && selects!=""){
			for(var j=0;j<selects.length;j++){
				var k=j;
				var idk=field.substring(1)+k;
				$("#multiSelect_"+idk).remove();
				$(selects[k]).attr("mt_val",k);
				$(selects[k]).before(multiHtml);
				var options=$(selects[k]).children("option");
				var multiSelect=$(selects[k]).prev();
				$(multiSelect).attr("id","multiSelect_"+idk);
				$(multiSelect).children(".mt_select_ful").attr("id","mt_select_ful_"+idk);
				$("#mt_select_ful_"+idk).children(".mt_select_ul").attr("id","mt_select_ul_"+idk);
				$("#mt_select_ful_"+idk).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("id","mt_select_input_"+idk);
				$(multiSelect).children(".mt_option_container").attr("id","mt_option_container_"+idk);
				$("#mt_option_container_"+idk).children(".mt_option_ul").attr("id","mt_option_ul_"+idk);
				if(options!=null && options!=""){
					for(var i=0;i<options.length;i++){
						var flag=0;
						var val=$(options[i]).val();
						var name=$(options[i]).html();
						if(!val){
							val=name;
						}
						if(selvalues){
							for(var j=0;j<selvalues.length;j++){
								if(selvalues[j]==val){
									flag=1;
								}
							}
						}
						if(flag==0){
							var li='<li class="mt_option_li">'+
								'<label val="'+val+'" index="'+i+'" class="mt_option_val">'+name+'</label>'+
							'</li>';
							$(multiSelect).children("div").eq(1).children("ul").append(li); 
						}
					}
				}
				if(placeholder){
					$("#mt_select_ful_"+idk).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder",placeholder);
				}
				if(sel_txt){
					$("#mt_select_ful_"+idk).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").width(20);
					$("#mt_select_ful_"+idk).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder","");
				}
				var mheight;
				if(style){
					var width=style.width;
					var height=style.height;
					var border=style.border;
					mheight=style.height;
					if(border){
						$("#multiSelect_"+idk).css("border",border);
						$("#mt_option_container_"+idk).css("border",border);
						$("#mt_option_container_"+idk).css("border-top","none");
					}
					if(width){
						$("#multiSelect_"+idk).css("width",width);
					}
					if(height){
						$("#multiSelect_"+idk+" .mt_select_ul li").css("height",height);
						$("#multiSelect_"+idk+" .mt_select_ul li").css("line-height",height);
						$("#mt_option_container_"+idk+" .mt_option_ul li").css("height",height);
						$("#mt_option_container_"+idk+" .mt_option_ul li").css("line-height",height);
						var mtop=6+(parseInt(height.substring(0,height.length-2))-26)/2;
						$("#multiSelect_"+idk+" .mt_select_ul li a").css("margin-top",mtop+"px");

					}
				}
				bindFunc(idk,mheight);
			}
		}
	},multiVal:function(){
		var select=this.selector;
		var valArr;
		if (select){
			valArr=new Array();
			var k=$(select).attr("mt_val");
			var idk=select.substring(1)+k;
			var labels=$("#mt_select_ul_"+idk+" li label");
			for (var i =0;i< labels.length; i++) {
				var val=$(labels[i]).attr("val");
				valArr.push(val);
			}
		}
		return valArr;
	},multiText:function(){
		var select=this.selector;
		var valArr;
		if (select){
			valArr=new Array();
			var k=$(select).attr("mt_val");
			var idk=select.substring(1)+k;
			var labels=$("#mt_select_ul_"+idk+" li label");
			for (var i =0;i< labels.length; i++) {
				var val=$(labels[i]).text();
				valArr.push(val);
			}
		}
		return valArr;
	}
});
function bindFunc(k,height){
	var no_match_label='<label class="no_match_label">没有找到匹配项</label>';
	$(".mt_select_container").on("click","#mt_select_ul_"+k+" li .mt_select_close",function(){
		$(this).parent().remove();
		var name=$(this).parent().children("label").eq(0).html();
		var val=$(this).parent().children("label").eq(0).attr("val");
		var li='<li class="mt_option_li">'+
					'<label val="'+val+'" class="mt_option_val">'+name+'</label>'+
				'</li>';
		$("#mt_option_ul_"+k).append(li); 
		$("#mt_option_container_"+k).hide();
		$("#mt_select_input_"+k).focus();
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").width(20);
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder","");
		$("#mt_option_ul_"+k +" .no_match_label").remove();
		$("#mt_option_ul_"+k +" .mt_option_li_label").show();

		if(height){
			$("#multiSelect_"+k+" .mt_select_ul li").css("height",height);
			$("#multiSelect_"+k+" .mt_select_ul li").css("line-height",height);
			$("#mt_option_container_"+k+" .mt_option_ul li").css("height",height);
			$("#mt_option_container_"+k+" .mt_option_ul li").css("line-height",height);
			var mtop=6+(parseInt(height.substring(0,height.length-2))-26)/2;
			$("#multiSelect_"+k+" .mt_select_ul li a").css("margin-top",mtop+"px");
		}
		return false;
	});

	$(".mt_select_container").on("click","#mt_select_input_"+k,function(){
		var disStyle =$("#mt_option_container_"+k).css("display")
		if(disStyle == "block"){
			$("#mt_option_container_"+k).hide();
			return;
		}
		$(".mt_option_container").hide();
		$("#mt_option_container_"+k).show();
		$(this).focus();
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").width(20);
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder","");
		var option=$(".mt_option_container #mt_option_ul_"+k+" li");
		if(option && option.length==0 || !option){
			$("#mt_option_ul_"+k +" .no_match_label").remove();
			$("#mt_option_container_"+k).children("ul").append(no_match_label);
			$("#mt_option_ul_"+k +" .no_match_label").show();
			$("#mt_option_ul_"+k +" .mt_option_li_label").hide();
		}
	});
	$(".mt_select_container").on("click","#mt_select_ful_"+k+" .mt_select_ul .mt_select_active",function(){
		var disStyle =$("#mt_option_container_"+k).css("display")
		if(disStyle == "block"){
			$("#mt_option_container_"+k).hide();
			return;
		}else{
			$("#mt_option_container_"+k).show();
		}
	});

	$(".mt_select_container").on("click","#mt_select_ful_"+k,function(){
		var disStyle =$("#mt_option_container_"+k).css("display")
		if(disStyle == "block"){
			$("#mt_option_container_"+k).hide();
			return;
		}
		$(".mt_option_container").hide();
		$("#mt_option_container_"+k).show();
		$("#mt_select_input_"+k).focus();
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").width(20);
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder","");
		var option=$(".mt_option_container #mt_option_ul_"+k+" li");
		if(option && option.length==0 || !option){
			$("#mt_option_ul_"+k +" .no_match_label").remove();
			$("#mt_option_container_"+k).children("ul").append(no_match_label);
			$("#mt_option_ul_"+k +" .no_match_label").show();
			$("#mt_option_ul_"+k +" .mt_option_li_label").hide();
		}
	});

	$(".mt_select_container").on("click","#mt_option_ul_"+k+" .mt_option_li",function(){
		$(this).remove();
		var name=$(this).children("label").eq(0).html();
		var val=$(this).children("label").eq(0).attr("val");
		var li='<li class="mt_select_li">'+
				'<a class="mt_select_close" href="javascript:;"></a>'+
				'<label val="'+val+'">'+name+'</label>'+
			'</li>';
		$("#mt_select_ul_"+k+" .mt_select_active").before(li);
		$("#mt_option_container_"+k).hide();
		$("#mt_select_input_"+k).focus();
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").width(20);
		$("#mt_select_ful_"+k).children(".mt_select_ul").children(".mt_select_active").children(".mt_select_input").attr("placeholder","");
		if(height){
			$("#multiSelect_"+k+" .mt_select_ul li").css("height",height);
			$("#multiSelect_"+k+" .mt_select_ul li").css("line-height",height);
			$("#mt_option_container_"+k+" .mt_option_ul li").css("height",height);
			$("#mt_option_container_"+k+" .mt_option_ul li").css("line-height",height);
			var mtop=6+(parseInt(height.substring(0,height.length-2))-26)/2;
			$("#multiSelect_"+k+" .mt_select_ul li a").css("margin-top",mtop+"px");
		}
	});
}
$(document).on("click",".no_match_label",function(){
	$(this).parent().parent().hide();
});


