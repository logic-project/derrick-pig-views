var navs = [
{
	"title": "系统管理",
	"icon": "&#xe614;",
	"spread": true,
	"children": [{
		"title": "用户管理",
		"icon": "&#xe66f;",
		"href": "../../main/system/users/usersList.html"
	},
	{
		"title": "角色管理",
		"icon": "&#xe770;",
		"href": "../../main/system/roles/roleList.html"
	},
	{
		"title": "权限管理",
		"icon": "&#xe672;",
		"href": "../../main/system/module/moduleManage.html"
	},
	{
		"title": "模块管理",
		"icon": "&#xe653;",
		"href": "../../main/system/module/moduleTreeList.html"
	},
	{
		"title": "系统日志",
		"icon": "&#xe60a;",
		"href": "../../main/system/sysLogs/sysLogList.html"
	},
	{
		"title": "数据字典",
		"icon": "&#xe630;",
		"href": "../../main/system/dictInfo/dictInfoList.html"
	}]
}];